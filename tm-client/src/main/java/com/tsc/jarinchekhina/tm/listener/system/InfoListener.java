package com.tsc.jarinchekhina.tm.listener.system;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import com.tsc.jarinchekhina.tm.util.NumberUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class InfoListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String name() {
        return "system-info";
    }

    @NotNull
    @Override
    public String description() {
        return "display system information";
    }

    @Override
    @EventListener(condition = "@infoListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

}
