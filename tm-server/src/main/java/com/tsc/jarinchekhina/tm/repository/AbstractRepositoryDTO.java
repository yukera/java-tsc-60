package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.dto.AbstractEntityDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepositoryDTO<E extends AbstractEntityDTO> {

    @NotNull
    @PersistenceContext
    private EntityManager entityManager;

    @NotNull
    public abstract List<E> findAll();

    public abstract void add(@NotNull final E entity);

    @NotNull
    public abstract E findById(@NotNull final String id);

    public abstract void clear();

    public abstract void removeById(@NotNull final String id);

    public abstract void remove(@NotNull final E entity);

}
