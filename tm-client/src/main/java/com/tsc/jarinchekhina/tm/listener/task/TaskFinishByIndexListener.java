package com.tsc.jarinchekhina.tm.listener.task;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractTaskListener;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskFinishByIndexListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "finish task by index";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskFinishByIndexListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskEndpoint().finishTaskByIndex(serviceLocator.getSession(), index);
    }

}
