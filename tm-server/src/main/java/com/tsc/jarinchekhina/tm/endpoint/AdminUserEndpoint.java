package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IAdminUserEndpoint;
import com.tsc.jarinchekhina.tm.api.service.ISessionService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
@NoArgsConstructor
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserDTO findByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.ADMIN);
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return User.toDTO(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserDTO lockByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.ADMIN);
        return User.toDTO(userService.lockByLogin(login));
    }

    @Override
    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.ADMIN);
        userService.removeByLogin(login);
    }

    @NotNull
    @Override
    @WebMethod
    public UserDTO unlockByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.ADMIN);
        return User.toDTO(userService.unlockByLogin(login));
    }

}
