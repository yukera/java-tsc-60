package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Autowired
    public ITaskRepository taskRepository;

    @Override
    @Transactional
    public void add(@NotNull final Task task) {
        taskRepository.add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Task task) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(userId, task);
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<Task> collection) {
        taskRepository.addAll(collection);
    }

    @Override
    @Transactional
    public void update(@NotNull final Task task) {
        taskRepository.update(task);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        taskRepository.clear(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        return taskRepository.findAll(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task findById(@NotNull final String id) {
        return taskRepository.findById(id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final Task task) {
        taskRepository.remove(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) return;
        taskRepository.removeById(userId, task.getId());
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String id) {
        taskRepository.removeById(id);
    }

    @Override
    @Transactional
    public void removeAllByProjectId(@NotNull final String projectId) {
        taskRepository.removeAllByProjectId(projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        taskRepository.removeById(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        @Nullable final Task task = taskRepository.findByIndex(userId, index);
        taskRepository.removeById(userId, task.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        taskRepository.removeByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Task task = findById(userId, id);
        task.setName(name);
        task.setDescription(description);
        taskRepository.update(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Task task = findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        taskRepository.update(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByIndex(@Nullable final String userId, final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startTaskById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findById(userId, id);
        changeTaskStatus(userId, task, status);
    }

    @SneakyThrows
    @Transactional
    public void changeTaskStatus(
            @NotNull final String userId,
            @NotNull final Task task,
            @NotNull final Status status
    ) {
        task.setStatus(status);
        taskRepository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startTaskByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findByIndex(userId, index);
        changeTaskStatus(userId, task, status);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeTaskStatusByName(userId, name, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findByName(userId, name);
        changeTaskStatus(userId, task, status);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishTaskById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishTaskByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        changeTaskStatusByIndex(userId, index, Status.COMPLETED);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeTaskStatusByName(userId, name, Status.COMPLETED);
    }

}
