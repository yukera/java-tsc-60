package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.model.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    @PersistenceContext
    private EntityManager entityManager;

    @NotNull
    @Override
    public abstract List<E> findAll();

    @Override
    public abstract void add(@NotNull final E entity);

    @NotNull
    @Override
    public abstract E findById(@NotNull final String id);

    @Override
    public abstract void clear();

    @Override
    public abstract void removeById(@NotNull final String id);

    @Override
    public abstract void remove(@NotNull final E entity);

}
