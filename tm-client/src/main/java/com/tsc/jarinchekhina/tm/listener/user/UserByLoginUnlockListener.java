package com.tsc.jarinchekhina.tm.listener.user;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractUserListener;
import com.tsc.jarinchekhina.tm.endpoint.AdminUserEndpoint;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class UserByLoginUnlockListener extends AbstractUserListener {

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "unlock user by login";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@userByLoginUnlockListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        adminUserEndpoint.unlockByLogin(serviceLocator.getSession(), login);
    }

}
