package com.tsc.jarinchekhina.tm.listener.task;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractTaskListener;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskFinishByNameListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "finish task by name";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskFinishByNameListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        getTaskEndpoint().finishTaskByName(serviceLocator.getSession(), name);
    }

}
