package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.other.ISaltSetting;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.env.Environment;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationDeveloper();

    @NotNull
    String getApplicationDeveloperEmail();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getJdbcPassword();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcUsername();

    @NotNull
    String getHibernateDialect();

    @NotNull
    String getHibernateHbm2ddl();

    @NotNull
    String getHibernateShow();

    @NotNull
    String getHibernateLazyLoad();

    @NotNull
    String getHibernateSecondLevelCache();

    @NotNull
    String getHibernateQueryCache();

    @NotNull
    String getHibernateMinimalPuts();

    @NotNull
    String getHibernateLiteMember();

    @NotNull
    String getHibernateRegionPrefix();

    @NotNull
    String getHibernateConfigFile();

    @NotNull
    String getHibernateRegionFactoryClass();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getApplicationVersion();

}
