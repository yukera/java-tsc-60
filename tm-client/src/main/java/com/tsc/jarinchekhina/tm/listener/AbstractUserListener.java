package com.tsc.jarinchekhina.tm.listener;

import com.tsc.jarinchekhina.tm.endpoint.UserDTO;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Getter
@Component
public abstract class AbstractUserListener extends AbstractListener {

    public void print(@NotNull final UserDTO user) {
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
    }

}
