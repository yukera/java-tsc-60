package com.tsc.jarinchekhina.tm.listener.user;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractUserListener;
import com.tsc.jarinchekhina.tm.endpoint.SessionEndpoint;
import com.tsc.jarinchekhina.tm.endpoint.UserDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class UserShowProfileListener extends AbstractUserListener {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-show";
    }

    @NotNull
    @Override
    public String description() {
        return "show user profile";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@userShowProfileListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[USER PROFILE]");
        @NotNull final UserDTO user = sessionEndpoint.getUser(serviceLocator.getSession());
        print(user);
    }

}
