package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    public IProjectRepository projectRepository;

    @Override
    @Transactional
    public void add(@NotNull final Project project) {
            projectRepository.add(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Project project) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(userId, project);
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<Project> collection) {
        projectRepository.addAll(collection);
    }

    @Override
    @Transactional
    public void update(@NotNull final Project project) {
        projectRepository.update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        projectRepository.clear(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        return projectRepository.findAll(userId);
    }

    @NotNull
    @Override
    public Project findById(@NotNull final String id) {
        return projectRepository.findById(id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final Project project) {
        projectRepository.remove(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final Project project) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) return;
        projectRepository.removeById(userId, project.getId());
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String id) {
        projectRepository.removeById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        projectRepository.removeById(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        @Nullable final Project project = projectRepository.findByIndex(userId, index);
        projectRepository.removeById(userId, project.getId());

    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        projectRepository.removeByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        projectRepository.update(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = findByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        projectRepository.update(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return projectRepository.findByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startProjectById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Project project = findById(userId, id);
        changeProjectStatus(userId, project, status);
    }

    @SneakyThrows
    @Transactional
    public void changeProjectStatus(
            @NotNull final String userId,
            @NotNull final Project project,
            @NotNull final Status status
    ) {
        project.setStatus(status);
        projectRepository.update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Project project = findByIndex(userId, index);
        changeProjectStatus(userId, project, status);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeProjectStatusByName(userId, name, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeProjectStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Project project = findByName(userId, name);
        changeProjectStatus(userId, project, status);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishProjectById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        changeProjectStatusByIndex(userId, index, Status.COMPLETED);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeProjectStatusByName(userId, name, Status.COMPLETED);
    }

}
