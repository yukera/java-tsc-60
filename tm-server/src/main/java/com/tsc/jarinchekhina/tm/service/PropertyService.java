package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Getter
@Setter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {
    
    @Value("${application.developer}")
    private String applicationDeveloper;
    
    @Value("${application.developer.email}")
    private String applicationDeveloperEmail;

    @Value("${application.version}")
    private String applicationVersion;
    
    @Value("${jdbc.driver}")
    private String jdbcDriver;
    
    @Value("${jdbc.url}")
    private String jdbcUrl;
    
    @Value("${jdbc.username}")
    private String jdbcUsername;

    @Value("${jdbc.password}")
    private String jdbcPassword;

    @Value("${hibernate.dialect}")
    private String hibernateDialect;
    
    @Value("${hibernate.hbm2ddl}")
    private String hibernateHbm2ddl;

    @Value("${hibernate.show}")
    private String hibernateShow;

    @Value("${hibernate.lazyload}")
    private String hibernateLazyLoad;

    @Value("${hibernate.secondlevelcache}")
    private String hibernateSecondLevelCache;
    
    @Value("${hibernate.querycache}")
    private String hibernateQueryCache;
    
    @Value("${hibernate.minimalputs}")
    private String hibernateMinimalPuts;

    @Value("${hibernate.litemember}")
    private String hibernateLiteMember;

    @Value("${hibernate.regionprefix}")
    private String hibernateRegionPrefix;

    @Value("${hibernate.configfile}")
    private String hibernateConfigFile;
    
    @Value("${hibernate.regionfactoryclass}")
    private String hibernateRegionFactoryClass;
    
    @Value("${password.iteration}")
    private Integer passwordIteration;

    @Value("${password.secret}")
    private String passwordSecret;

    @Value("${server.host:localhost}")
    private String serverHost;

    @Value("${server.port:8080}")
    private Integer serverPort;

    @Value("${session.iteration}")
    private Integer sessionIteration;

    @Value("${session.secret}")
    private String sessionSecret;

}
